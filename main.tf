//variable "AWS_SECRET_ACCESS_KEY" {}
//variable "AWS_ACCESS_KEY_ID" {}

provider "aws" {
  region = "us-east-2"
}

resource "aws_instance" "molecula_node" {
  ami           = "ami-0a0ad6b70e61be944"
  instance_type = "t2.micro" //Is A Free instance
  tags = {
    Name = "Molecula node"
  }
  key_name="AWS_for_molecula"
  vpc_security_group_ids = [ aws_security_group.molecula_security_group.id ]
  //user_data = templatefile("deploy.sh", {public_ip_for_Deploy = "I realy dont know what will be here than"})
  }
  
resource "aws_key_pair" "AWS" {
  key_name   = "AWS_for_molecula"
  public_key = file("./Stuff/Key.pub")                  //give servers Public Key
}
resource "aws_security_group" "molecula_security_group" {
  name        = "Molecula Security Group"
  description = "To allow Web Access"

  dynamic "ingress" {
    for_each = ["80", "443", "22", "8080", "9000"] //Allow ports for Nginx, Jenkins, SSH
    content {
      from_port   = ingress.value
      protocol    = "tcp"
      to_port     = ingress.value
      cidr_blocks = ["0.0.0.0/0"] //allow to every IP  from outside network 
    }
  }

  egress {
    from_port   = 0 //Every traffic to every IP to outter network is avaliable
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
//resource "null_resource" "Deploy" {
//  depends_on = aws_instance.Deploy
//  provisioner "local-exec" {
//    command = "echo ${aws_instance.Deploy.public_ip} >> /Stuff/hosts"
//  }
//}
output "public_ip_for_Deploy" {     //output public IP for Deploy server
  value = aws_instance.molecula_node.public_ip
}



